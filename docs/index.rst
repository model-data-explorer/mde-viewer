.. mde-viewer documentation master file, created by
   sphinx-quickstart on Mon Feb 21 15:15:53 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mde-viewer's documentation!
======================================

.. rubric:: A Map Frontend for the Model Data Explorer with AngularJS and ArcGIS for Javascript

.. warning::

    This package is work in progress, especially it's documentation.
    Stay tuned for updates and discuss with us at
    https://gitlab.hzdr.de/model-data-explorer/mde-viewer


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   configuration
   api
   contributing



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
