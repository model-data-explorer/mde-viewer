import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColorPickerModule } from 'ngx-color-picker';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { InputNumberModule } from 'primeng/inputnumber';
import { SplitButtonModule } from 'primeng/splitbutton';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { SplitterModule } from "primeng/splitter";
import { HomepageComponent } from './components/homepage/homepage.component';
import { ObjectListsComponent } from './components/object-lists/object-lists.component';
import { TabViewModule } from 'primeng/tabview';
import { ModelRunMetadataComponent } from './components/model-run-metadata/model-run-metadata.component';
import { EsriMapComponent } from './components/esri-map/esri-map.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LayerManagerComponent } from './components/layer-manager/layer-manager.component';
import { LayerEditorComponent } from './components/layer-editor/layer-editor.component';


@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    ObjectListsComponent,
    ModelRunMetadataComponent,
    EsriMapComponent,
    DashboardComponent,
    LayerManagerComponent,
    LayerEditorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ButtonModule,
    InputTextModule,
    InputNumberModule,
    FormsModule,
    SplitButtonModule,
    BrowserAnimationsModule,
    DialogModule,
    DropdownModule,
    SplitterModule,
    TabViewModule,
    ColorPickerModule,
    CommonModule,
    OverlayPanelModule,
    RouterModule.forRoot([
      { path: 'homepage', component: HomepageComponent },
      { path: '', redirectTo: '/homepage', pathMatch: 'full' }
    ])
  ],
  exports: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
