import { Legend } from './../../_interface/legend';
import { ColorPallete } from './../../_interface/color-pallete';
import { Component, OnInit, SystemJsNgModuleLoader, ViewEncapsulation } from '@angular/core';
import { EsriMapComponent } from '../esri-map/esri-map.component';
import { SelectItem } from 'primeng/api';




@Component({
  selector: 'app-layer-editor',
  templateUrl: './layer-editor.component.html',
  styleUrls: ['./layer-editor.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class LayerEditorComponent {

  layerTypes: any = [];
  selectedLayerType: string;
  legends: [Legend] = [{ color: "rgb(85, 109, 227, 0.6)", startLimit: 1, endLimit: 2 }];

  colorEndInfinate: string = "rgb(246, 0, 0, 0.6)";
  colorStartInfinate: string = "rgb(246, 0, 0, 0.6)";
  checkedInfinityStart: boolean = false;
  checkedInfinityEnd: boolean = false;

  colorPalletes: ColorPallete[];
  selectedPallet: ColorPallete;
  items: SelectItem[];

  item: string;

  constructor(private esriMapComponent: EsriMapComponent) {

    this.layerTypes = [
      { name: 'Imagery Layer', code: 'IMG' },
      { name: 'WMS Layer', code: 'WMS' }
    ];

    this.colorPalletes = [
      { name: 'Pallete 1', code: 'P1', inactive: false },
      { name: 'Pallete 2', code: 'P2', inactive: false },
      { name: 'Pallete 3', code: 'P3', inactive: false },
      { name: 'Pallete 4', code: 'P4', inactive: false },
      { name: 'Pallete 5', code: 'P5', inactive: false },
      { name: 'Pallete 6', code: 'P6', inactive: false },
      { name: 'Pallete 7', code: 'P7', inactive: false },
      { name: 'Pallete 8', code: 'P8', inactive: false },
      { name: 'Pallete 9', code: 'P9', inactive: false },
      { name: 'Pallete 10', code: 'P10', inactive: false }
    ];



  }


  display: boolean;
  displayPosition: boolean;
  position: string;
  public esriLayerServiceURL: String;

  showMaximizableDialog() {
    this.display = true;
  }

  public addNewLegendRow() {
    let legend1: Legend = { color: "rgb(85, 109, 227, 0.6)", startLimit: 1, endLimit: 2 };

    this.legends.push(legend1);
  }

  public removeLastLegendRow() {
    this.legends.pop();
  }


  showPositionDialog(position: string) {
    this.position = position;
    this.displayPosition = true;
  }

  public updateLayer() {
    alert("Are you sure you want to add this layer? : " + this.esriLayerServiceURL);
    this.esriMapComponent.updateLayer(this.esriLayerServiceURL)

  }

  public addLayer() {
    alert("Are you sure you want to add this layer? : " + this.esriLayerServiceURL);
    this.esriMapComponent.addLayer(this.esriLayerServiceURL)

  }



}
