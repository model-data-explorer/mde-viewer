import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectListsComponent } from './object-lists.component';

describe('ObjectListsComponent', () => {
  let component: ObjectListsComponent;
  let fixture: ComponentFixture<ObjectListsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObjectListsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
