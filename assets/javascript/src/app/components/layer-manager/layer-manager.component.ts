import { EsriMapComponent } from './../esri-map/esri-map.component';
import { Component, ViewEncapsulation } from '@angular/core';


@Component({
  selector: 'app-layer-manager',
  templateUrl: './layer-manager.component.html',
  styleUrls: ['./layer-manager.component.scss'],
  encapsulation:ViewEncapsulation.Emulated
})
export class LayerManagerComponent  {
  layerTypes : any = [];

  selectedLayerType: string;

  constructor( private esriMapComponent: EsriMapComponent  ) {

    this.layerTypes = [
      {name: 'Imagery Layer', code: 'IMG'},
      {name: 'WMS Layer', code: 'WMS'}
    ]

  }

  displayMaximizable: boolean;
  displayPosition: boolean;
  position: string;

  public esriLayerServiceURL: String;


  showMaximizableDialog() {
      this.displayMaximizable = true;
  }

  showPositionDialog(position: string) {
      this.position = position;
      this.displayPosition = true;
  }

  public updateLayer(){
    alert("Are you sure you want to add this layer? : "+ this.esriLayerServiceURL);
    this.esriMapComponent.updateLayer(this.esriLayerServiceURL)

  }

  public addLayer(){
    alert("Are you sure you want to add this layer? : "+ this.esriLayerServiceURL);
    this.esriMapComponent.addLayer(this.esriLayerServiceURL)
  }


}
