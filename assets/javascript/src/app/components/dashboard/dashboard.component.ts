import { Component, ViewChild, OnInit } from '@angular/core';
import { EsriMapComponent } from '../esri-map/esri-map.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent   {

  @ViewChild(EsriMapComponent, { static: true }) map: EsriMapComponent; // needed to reference the child map component


  listOfLocations = [
    {id: 0, name: 'Nordsea', coordinates: [3.119205,56.237817]},
    {id: 1, name: 'Geesthacht', coordinates: [10.1234, 55.5174]},
    {id: 2, name: 'Hamburg', coordinates: [10.1234, 55.5174]},
    {id: 3, name: 'Ostsee', coordinates: [10.1234, 55.5174]},
  ];

  feedback: any;
  selectorDisabled = false;

  selectedWonder (ev)  {
    // verify that a wonder is selected
    if (ev.target.value === '') {
      return;
    }

    // disable the panel
    this.disablePanel(this.listOfLocations[ev.target.value].name);

    // call the panMap method of the child map component
    this.map.panMap(this.listOfLocations[ev.target.value].coordinates);

  }

  disablePanel (name)  {
    this.selectorDisabled = true;
    this.feedback = this.feedback = 'Panning to ' + name + '.';
  }

  enablePanel = () => {
    this.selectorDisabled = false;
    this.feedback = 'Done!';
    setTimeout(() => { this.feedback = ''; }, 1000);
  };



}
