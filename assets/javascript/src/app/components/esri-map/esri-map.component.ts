import { PrimeNGConfig } from 'primeng/api';
import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { loadModules } from 'esri-loader';
import { LayerEditorComponent } from '../layer-editor/layer-editor.component';


@Component({
  selector: 'app-esri-map',
  templateUrl: './esri-map.component.html',
  styleUrls: ['./esri-map.component.scss']
})

export class EsriMapComponent implements OnInit {

  @Output() wonderMapped = new EventEmitter(); // notfies the dashboard component when the map is finished
  @ViewChild('mapViewNode', { static: true }) private viewNode: ElementRef; // needed to inject the MapView into the DOM
  mapView: __esri.MapView;


  color1: string = "#1976D2";
  color2: string = "#1976D2";
  @ViewChild(LayerEditorComponent) layerEditorComponent: LayerEditorComponent;

  value1: string;
  map: __esri.Map;

  constructor(private primengConfig: PrimeNGConfig) {
  }

  panMap = (coordinates) => {
    this.mapView.goTo(coordinates)
      .then(() => {
        this.mapView.zoom = 18;
        setTimeout(() => {
          this.wonderMapped.emit();
        }, 2000);
      });
  }


  public ngOnInit() {
    this.primengConfig.ripple = true;

    return loadModules([
      'esri/Map',
      'esri/views/MapView',
      'esri/Graphic',
      'esri/layers/ImageryLayer',
      'esri/widgets/Compass',
      'esri/widgets/BasemapToggle',
      'esri/widgets/CoordinateConversion',
      'esri/widgets/BasemapLayerList',
      'esri/widgets/Fullscreen',
      'esri/widgets/Legend',
      'esri/widgets/LayerList',
      'esri/layers/MapImageLayer',
      'esri/renderers/Renderer',
      'esri/layers/support/RasterFunction',
      'esri/widgets/Search',

      'esri/renderers/SimpleRenderer',
      'esri/smartMapping/renderers/color',
      'esri/renderers/RasterColormapRenderer',
      'esri/renderers/ClassBreaksRenderer',
      'esri/symbols/SimpleFillSymbol',
      'esri/Color',
      'esri/layers/WMSLayer',
      'esri/geometry/Extent',
      'esri/widgets/Slider',
      'esri/renderers/RasterStretchRenderer',
      'esri/widgets/smartMapping/ColorSlider'

    ])
      .then(async ([Map, MapView, Graphic, ImageryLayer, Compass, BasemapToggle, CoordinateConversion, BasemapLayerList, Fullscreen,
        Legend, LayerList, MapImageLayer, Renderer, RasterFunction, Search, SimpleRenderer, colorRendererCreator, RasterColormapRenderer, ClassBreaksRenderer,
        SimpleFillSymbol, Color, WMSLayer, Extent, Slider, RasterStretchRenderer, ColorSlider]) => {

        this.map = new Map({
          basemap: 'topo-vector'

        });


        this.mapView = new MapView({
          container: this.viewNode.nativeElement,

          map: this.map,
          zoom: 4,
          center: [15, 65] // longitude, latitude
        });

        const imagePopupTemplate = {
          // autocasts as new PopupTemplate()
          title: "Data from {SensorName} satellite",
          content: `
            Rendered RGB values: <b>{Raster.ServicePixelValue} </b>
            <br>Original values (B,  G, R, NIR): <b>{Raster.ItemPixelValue} </b>
            <br>Legend: <b>{Raster.Colormap.value} </b>
            `
        };


        const less35 = {
          type: "simple-fill", // autocasts as new SimpleFillSymbol()
          color: "#fffcd4",
          style: "solid",
          outline: {
            width: 0.2,
            color: [255, 255, 255, 0.5]
          }
        };

        const less50 = {
          type: "simple-fill", // autocasts as new SimpleFillSymbol()
          color: "#b1cdc2",
          style: "solid",
          outline: {
            width: 0.2,
            color: [255, 255, 255, 0.5]
          }
        };

        const more50 = {
          type: "simple-fill", // autocasts as new SimpleFillSymbol()
          color: "#38627a",
          style: "solid",
          outline: {
            width: 0.2,
            color: [255, 255, 255, 0.5]
          }
        };

        const more75 = {
          type: "simple-fill", // autocasts as new SimpleFillSymbol()
          color: "#ee0909",
          style: "solid",
          outline: {
            width: 0.2,
            color: [255, 0, 0, 0.5]
          }
        };

        const renderer = {
          type: "class-breaks", // autocasts as new ClassBreaksRenderer()

          normalizationField: "EDUCBASECY",
          legendOptions: {
            title: "% of adults (25+) with a college degree"
          },
          defaultSymbol: {
            type: "simple-fill", // autocasts as new SimpleFillSymbol()
            color: "black",
            style: "backward-diagonal",
            outline: {
              width: 0.5,
              color: [50, 50, 50, 0.6]
            }
          },
          defaultLabel: "no data",
          classBreakInfos: [
            {
              minValue: 0,
              maxValue: 0.3499,
              symbol: less35,
              label: "< 35%"
            },
            {
              minValue: 0.35,
              maxValue: 0.4999,
              symbol: less50,
              label: "35 - 50%"
            },
            {
              minValue: 0.5,
              maxValue: 0.7499,
              symbol: more50,
              label: "50 - 75%"
            },
            {
              minValue: 0.75,
              maxValue: 1.0,
              symbol: more75,
              label: "> 75%"
            }
          ]
        };


        const layer = new ImageryLayer({
          url: "https://hub.hereon.de/server/rest/services/Data/HZG_coastmap_TOC_in_sediment/ImageServer",
          popupTemplate: imagePopupTemplate,
          renderer: renderer

        });


        this.map.add(layer);

        let compass = new Compass({
          view: this.mapView
        });
        this.mapView.ui.add(compass, "top-left");


        let fullscreen = new Fullscreen({
          view: this.mapView
        });
        this.mapView.ui.add(fullscreen, "top-right");

        let basemapToggle = new BasemapToggle({
          view: this.mapView,
          nextBasemap: "hybrid"
        });
        this.mapView.ui.add(basemapToggle, "top-right");

        let legend = new Legend({
          view: this.mapView

        });

        this.mapView.ui.add(legend, "bottom-right");




        let layerList = new LayerList({
          view: this.mapView,
          listItemCreatedFunction: defineActionsForLayerList
        });



        function defineActionsForLayerList(event) {
          // The event object contains an item property.
          // is is a ListItem referencing the associated layer
          // and other properties. You can control the visibility of the
          // item, its title, and actions using this object.

          const item = event.item;



          item.actionsSections = [
            [
              {
                title: "Edit Layer",
                className: "esri-icon-edit",
                id: "edit-layer"
              },
              {
                title: "Go to full extent",
                className: "esri-icon-zoom-out-fixed",
                id: "full-extent"
              },
              {
                title: "Layer information",
                className: "esri-icon-description",
                id: "information"
              }
            ],
            [
              {
                title: "Move Up",
                className: "esri-icon-up-arrow",
                id: "move-layer-up"
              },
              {
                title: "Move Down",
                className: "esri-icon-down-arrow",
                id: "move-layer-down"
              }
            ],
            [
              {
                title: "Increase opacity",
                className: "esri-icon-up",
                id: "increase-opacity"
              },
              {
                title: "Decrease opacity",
                className: "esri-icon-down",
                id: "decrease-opacity"
              }
            ]
          ];

        };

        layerList.on("trigger-action", (event) => {
          // The layer visible in the view at the time of the trigger.
          let visibleLayer: any;

          const item = event.item;



          this.map.layers.forEach(element => {


            if (element.title == item.title) {

              visibleLayer = element
            }

          });



          // Capture the action id.
          const id = event.action.id;

          if (id === "full-extent") {

            this.mapView.goTo(visibleLayer.fullExtent);
          } else if (id === "information") {

            window.open(visibleLayer.url);
          } else if (id === "increase-opacity") {

            if (visibleLayer.opacity < 1) {
              visibleLayer.opacity += 0.25;
            }
          } else if (id === "decrease-opacity") {

            if (visibleLayer.opacity > 0) {
              visibleLayer.opacity -= 0.25;
            }
          } else if (id === "move-layer-up") {

            var layerToMoveName = event.item.title;
            var layerToMoveIdx = -1

            for (var i = 0; i < layerList.viewModel.operationalItems.length; i++) {
              var item1 = layerList.viewModel.operationalItems.getItemAt(i)
              if (item1.title === layerToMoveName) {
                layerToMoveIdx = i
              }
            }

            if (layerToMoveIdx > 0) {
              layerList.viewModel.moveListItem(event.item, null, null, layerToMoveIdx);
            }
          }
          else if (id === "move-layer-down") {

            var layerToMoveName = event.item.title;
            var layerToMoveIdx = -1

            for (var i = 0; i < layerList.viewModel.operationalItems.length; i++) {
              var item1 = layerList.viewModel.operationalItems.getItemAt(i)
              if (item1.title === layerToMoveName) {
                layerToMoveIdx = i
              }
            }

            if (layerToMoveIdx < layerList.viewModel.operationalItems.length) {
              layerList.viewModel.moveListItem(event.item, null, null, layerToMoveIdx + 2);
            }

          } else if (id === "edit-layer") {

            alert(visibleLayer.title);


            for (var i = 0; i < layerList.viewModel.operationalItems.length; i++) {
              var item1 = layerList.viewModel.operationalItems.getItemAt(i)

              if (item1.title === visibleLayer.legendUrl) {
                alert(item1.title);

              }
            }




            this.layerEditorComponent.showMaximizableDialog();
          }


        });

        const getCircularReplacer = () => {
          const seen = new WeakSet();
          return (key, value) => {
            if (typeof value === "object" && value !== null) {
              if (seen.has(value)) {
                return;
              }
              seen.add(value);
            }
            return value;
          };
        };


        this.mapView.ui.add(layerList, {
          position: "top-left"
        });



        const searchWidget = new Search({
          view: this.mapView
        });

        this.mapView.ui.add(searchWidget, {
          position: "top-left",
          index: 0
        });


        this.mapView.ui.add("rendererDiv", "top-left");





      })
      .catch(err => {
        console.log(err);
      });
  }

  public async updateLayer(newLayerUrl: String) {
    this.primengConfig.ripple = true;
    // use esri-loader to load JSAPI modules
    try {
      const [Map, MapView, Graphic, ImageryLayer, Compass, BasemapToggle, CoordinateConversion, BasemapLayerList, Fullscreen, Legend, LayerList, MapImageLayer, Renderer, RasterFunction, Search, SimpleRenderer, colorRendererCreator, RasterColormapRenderer, ClassBreaksRenderer, SimpleFillSymbol, Color] = await loadModules([
        'esri/Map',
        'esri/views/MapView',
        'esri/Graphic',
        'esri/layers/ImageryLayer',
        'esri/widgets/Compass',
        'esri/widgets/BasemapToggle',
        'esri/widgets/CoordinateConversion',
        'esri/widgets/BasemapLayerList',
        'esri/widgets/Fullscreen',
        'esri/widgets/Legend',
        'esri/widgets/LayerList',
        'esri/layers/MapImageLayer',
        'esri/renderers/Renderer',
        'esri/layers/support/RasterFunction',
        'esri/widgets/Search',
        'esri/renderers/SimpleRenderer',
        'esri/smartMapping/renderers/color',
        'esri/renderers/RasterColormapRenderer',
        'esri/renderers/ClassBreaksRenderer',
        'esri/symbols/SimpleFillSymbol',
        'esri/Color'
      ]);
      const map: __esri.Map = new Map({
        basemap: 'topo-vector'
      });


      this.mapView = new MapView({
        container: this.viewNode.nativeElement,
        center: [10.1234, 55.5174],


        zoom: 5,
        map: map
      });


      const imagePopupTemplate = {
        // autocasts as new PopupTemplate()
        title: "Data from {SensorName} satellite",
        content: `
            Rendered RGB values: <b>{Raster.ServicePixelValue} </b>
            <br>Original values (B,  G, R, NIR): <b>{Raster.ItemPixelValue} </b>
            <br>Legend: <b>{Raster.Colormap.value} </b>
            `
      };

      const less35 = {
        type: "simple-fill",
        color: "#fffcd4",
        style: "solid",
        outline: {
          width: 0.2,
          color: [255, 255, 255, 0.5]
        }
      };

      const less50 = {
        type: "simple-fill",
        color: "#b1cdc2",
        style: "solid",
        outline: {
          width: 0.2,
          color: [255, 255, 255, 0.5]
        }
      };

      const more50 = {
        type: "simple-fill",
        color: "#38627a",
        style: "solid",
        outline: {
          width: 0.2,
          color: [255, 255, 255, 0.5]
        }
      };

      const more75 = {
        type: "simple-fill",
        color: "#ee0909",
        style: "solid",
        outline: {
          width: 0.2,
          color: [255, 0, 0, 0.5]
        }
      };

      const renderer = {
        type: "class-breaks",

        normalizationField: "EDUCBASECY",
        legendOptions: {
          title: "% of adults (25+) with a college degree"
        },
        defaultSymbol: {
          type: "simple-fill",
          color: "black",
          style: "backward-diagonal",
          outline: {
            width: 0.5,
            color: [50, 50, 50, 0.6]
          }
        },
        defaultLabel: "no data",
        classBreakInfos: [
          {
            minValue: 0,
            maxValue: 0.3499,
            symbol: less35,
            label: "< 35%"
          },
          {
            minValue: 0.35,
            maxValue: 0.4999,
            symbol: less50,
            label: "35 - 50%"
          },
          {
            minValue: 0.5,
            maxValue: 0.7499,
            symbol: more50,
            label: "50 - 75%"
          },
          {
            minValue: 0.75,
            maxValue: 1,
            symbol: more75,
            label: "> 75%"
          }
        ]
      };

      const layer = new ImageryLayer({
        url: newLayerUrl,
        popupTemplate: imagePopupTemplate,
        renderer: renderer
      });


      this.map.add(layer);

      let compass = new Compass({
        view: this.mapView
      });
      this.mapView.ui.add(compass, "top-left");




      let fullscreen = new Fullscreen({
        view: this.mapView
      });
      this.mapView.ui.add(fullscreen, "top-right");

      let basemapToggle = new BasemapToggle({
        view: this.mapView,
        nextBasemap: "hybrid"
      });
      this.mapView.ui.add(basemapToggle, "top-right");

      let legend = new Legend({
        view: this.mapView
      });

      this.mapView.ui.add(legend, "bottom-right");

      let layerList = new LayerList({
        view: this.mapView
      });

      this.mapView.ui.add(layerList, {
        position: "top-left"
      });

      const searchWidget = new Search({
        view: this.mapView
      });

      this.mapView.ui.add(searchWidget, {
        position: "top-left",
        index: 0
      });




      this.mapView.ui.add("rendererDiv", "top-left");

      var o = layer.toJSON();
      console.log('***********' + JSON.stringify(o));
    } catch (err) {
      console.log(err);
    }
  }

  public async addLayer(newLayerUrl: String) {
    this.primengConfig.ripple = true;
    // use esri-loader to load JSAPI modules
    try {
      const [Map, MapView, Graphic, ImageryLayer, Compass, BasemapToggle, CoordinateConversion, BasemapLayerList, Fullscreen, Legend, LayerList, MapImageLayer, Renderer, RasterFunction, Search, SimpleRenderer, colorRendererCreator, RasterColormapRenderer, ClassBreaksRenderer, SimpleFillSymbol, Color, RasterStretchRenderer, AlgorithmicColorRamp] = await loadModules([
        'esri/Map',
        'esri/views/MapView',
        'esri/Graphic',
        'esri/layers/ImageryLayer',
        'esri/widgets/Compass',
        'esri/widgets/BasemapToggle',
        'esri/widgets/CoordinateConversion',
        'esri/widgets/BasemapLayerList',
        'esri/widgets/Fullscreen',
        'esri/widgets/Legend',
        'esri/widgets/LayerList',
        'esri/layers/MapImageLayer',
        'esri/renderers/Renderer',
        'esri/layers/support/RasterFunction',
        'esri/widgets/Search',
        'esri/renderers/SimpleRenderer',
        'esri/smartMapping/renderers/color',
        'esri/renderers/RasterColormapRenderer',
        'esri/renderers/ClassBreaksRenderer',
        'esri/symbols/SimpleFillSymbol',
        'esri/Color',
        'esri/renderers/RasterStretchRenderer'

      ]);



      const imagePopupTemplate = {
        // autocasts as new PopupTemplate()
        title: "Data from {SensorName} satellite",
        content: `
            Rendered RGB values: <b>{Raster.ServicePixelValue} </b>
            <br>Original values (B,  G, R, NIR): <b>{Raster.ItemPixelValue} </b>
            <br>Legend: <b>{Raster.Colormap.value} </b>
            `
      };

      const less35 = {
        type: "simple-fill",
        color: "#fffcd4",
        style: "solid",
        outline: {
          width: 0.2,
          color: [255, 255, 255, 0.5]
        }
      };

      const less50 = {
        type: "simple-fill",
        color: "#b1cdc2",
        style: "solid",
        outline: {
          width: 0.2,
          color: [255, 255, 255, 0.5]
        }
      };

      const more50 = {
        type: "simple-fill",
        color: "#38627a",
        style: "solid",
        outline: {
          width: 0.2,
          color: [255, 255, 255, 0.5]
        }
      };

      const more75 = {
        type: "simple-fill",
        color: "#ee0909",
        style: "solid",
        outline: {
          width: 0.2,
          color: [255, 0, 0, 0.5]
        }
      };

      const renderer = {
        type: "class-breaks",

        normalizationField: "EDUCBASECY",
        legendOptions: {
          title: "% of adults (25+) with a college degree"
        },
        defaultSymbol: {
          type: "simple-fill",
          color: "black",
          style: "backward-diagonal",
          outline: {
            width: 0.5,
            color: [50, 50, 50, 0.6]
          }
        },
        defaultLabel: "no data",
        classBreakInfos: [
          {
            minValue: 0,
            maxValue: 0.3499,
            symbol: less35,
            label: "< 35%"
          },
          {
            minValue: 0.35,
            maxValue: 0.4999,
            symbol: less50,
            label: "35 - 50%"
          },
          {
            minValue: 0.5,
            maxValue: 0.7499,
            symbol: more50,
            label: "50 - 75%"
          },
          {
            minValue: 0.75,
            maxValue: 1,
            symbol: more75,
            label: "> 75%"
          }
        ]
      };

      const layer = new ImageryLayer({
        url: newLayerUrl,
        renderer: renderer
      });

      console.log((String)(this.map));

      this.map.add(layer)


    } catch (err) {
      console.log(err);
    }
  }



}
