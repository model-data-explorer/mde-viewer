import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelRunMetadataComponent } from './model-run-metadata.component';

describe('ModelRunMetadataComponent', () => {
  let component: ModelRunMetadataComponent;
  let fixture: ComponentFixture<ModelRunMetadataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModelRunMetadataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelRunMetadataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
