export interface ColorPallete {
    name: string,
    code?: string,
    colors?: string[],
    inactive?: Boolean

}
