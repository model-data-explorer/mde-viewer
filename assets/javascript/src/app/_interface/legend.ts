export interface Legend {
    color ?: String;
    startLimit ?: Number;
    endLimit ?: Number;
    name ?: String;

}
