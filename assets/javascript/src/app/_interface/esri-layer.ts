import { Legend } from './legend';
export interface EsriLayer {
    layerId?: String;
    layerName?: Legend;
    layerURL?: String;
    layerType?: String;

}
