"""Views
-----

Views of the mde-viewer app to be imported via the url
config (see :mod:`mde_viewer.urls`).
"""

# Disclaimer
# ----------
#
# Copyright (C) 2022 Helmholtz-Zentrum Hereon
#
# This file is part of mde-viewer and is released under the
# EUPL-1.2 license.
# See LICENSE in the root of the repository for full licensing details.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 or later
# as published by the European Commission.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# EUPL-1.2 license for more details.
#
# You should have received a copy of the EUPL-1.2 license along with this
# program. If not, see https://www.eupl.eu/.


from __future__ import annotations

from django.views import generic  # noqa: F401
from rest_framework import generics

from mde_viewer import app_settings  # noqa: F401
from mde_viewer import models, serializers  # noqa: F401


class MapFrontendView(generic.TemplateView):

    template_name = "mde_viewer/viewer.html"


class DatasetAPIView(generics.RetrieveAPIView):

    serializer_class = serializers.DatasetSerializer

    queryset = models.Dataset.objects.all()
