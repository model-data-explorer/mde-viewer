"""Models
------

Models for the mde-viewer app.
"""

# Disclaimer
# ----------
#
# Copyright (C) 2022 Helmholtz-Zentrum Hereon
#
# This file is part of mde-viewer and is released under the
# EUPL-1.2 license.
# See LICENSE in the root of the repository for full licensing details.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 or later
# as published by the European Commission.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# EUPL-1.2 license for more details.
#
# You should have received a copy of the EUPL-1.2 license along with this
# program. If not, see https://www.eupl.eu/.


from __future__ import annotations

from django.contrib.postgres.fields import DateTimeRangeField
from django.db import models  # noqa: F401
from mde_core.models import Dataset

from mde_viewer import app_settings  # noqa: F401


class Service(models.Model):

    dataset = models.ForeignKey(
        Dataset,
        on_delete=models.CASCADE,
        null=True,
        help_text="The dataset that this service corresponds to.",
    )

    time_range = DateTimeRangeField(
        help_text="Minimum and maximum time step of the service"
    )


class ServiceLayer(models.Model):
    """A variable that can be visualized by a service"""

    name = models.CharField(max_length=200)

    description = models.TextField(
        null=True,
        blank=True,
        help_text="A more verbose description of the layer.",
    )

    service = models.ForeignKey(Service, on_delete=models.CASCADE)
