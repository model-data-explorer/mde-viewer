from mde_core.models import DataGroup, Dataset
from rest_framework import serializers

from . import models


class ServiceLayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ServiceLayer
        fields = "__all__"


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Service
        fields = "__all__"

    servicelayer_set = ServiceLayerSerializer(many=True)


class DataGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataGroup
        fields = "__all__"


class DatasetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dataset
        fields = "__all__"

    service_set = ServiceSerializer(many=True)
    list_datagroups = DataGroupSerializer(many=True)
