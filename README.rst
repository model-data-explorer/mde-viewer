================================
Model Data Explorer Map Frontend
================================

A Map Frontend for the Model Data Explorer with AngularJS and ArcGIS for Javascript

Installation
------------

Install this package in a dedicated python environment via::

    python -m venv venv
    source venv/bin/activate
    pip install mde-viewer

To use this in a development setup, clone the `source code`_ from gitlab, start
the development server and make your changes::

    git clone https://gitlab.hzdr.de/model-data-explorer/mde-viewer
    cd mde-viewer
    python -m venv venv
    source venv/bin/activate
    make dev-install

To build the frontend in development mode, you need angular to be installed and
run::

    cd assets/javascript
    npm install
    npm run-script watch

This will make an incremental build every time you save a file in the frontend
and deploy to the static files directory of the ``mde_viewer`` django app.

More detailed installation instructions my be found in the docs_.




.. _source code: https://gitlab.hzdr.de/model-data-explorer/mde-viewer
.. _docs: https://model-data-explorer.readthedocs.io/projects/mde-viewer/en/latest/installation.html


Copyright
---------
Copyright © 2022 Helmholtz-Zentrum Hereon

Licensed under the EUPL-1.2-or-later

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the EUPL-1.2 license for more details.

You should have received a copy of the EUPL-1.2 license along with this
program. If not, see https://www.eupl.eu/.
